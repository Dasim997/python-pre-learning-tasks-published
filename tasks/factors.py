def factors(x):
    # ==============
    # Your code here
    l = []
    for i in range(2, x - 1):
        if x % i == 0:
            l.append(i)
    return l
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
